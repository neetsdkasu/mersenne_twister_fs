﻿(* Mersenne Twister 移植
 * Leonardone @ NEETSDKASU
 * BSD-2-Clause License
 *)
namespace Mt19937ar

module private Binds =
    let [<Literal>] N = 624
    let [<Literal>] M = 397
    let [<Literal>] MatrixA = 0x9908b0dfu
    let [<Literal>] UpperMask = 0x80000000u
    let [<Literal>] LowerMask = 0x7fffffffu
    let [<Literal>] DefaultSeed = 5489u
    let mag01 = [| 0u; MatrixA |]

    let inline genArray (i, v) =
        if i < uint N then
            Some (v, (i + 1u, 1812433253u * (v ^^^ (v >>> 30)) + i))
        elif i = uint N then
            Some (v, (i + 1u, v))
        else
            None

    (* initializes mt[N] with a seed *)
    let inline initBySeed seed = Array.unfold genArray (1u, seed)

    (* initialize by an array *)
    let inline mergeKey mt key =
        let mutable i = 1
        let mutable j = 0
        for k = 1 to max N (Array.length key) do
            Array.set mt i <|
                (Array.get mt i ^^^
                    ((Array.get mt (i - 1) ^^^
                        (Array.get mt (i - 1) >>> 30)
                ) * 1664525u)) + Array.get key j + uint j
            if i + 1 < N then
                i <- i + 1
            else
                i <- 1
                Array.set mt 0 <| Array.last mt
            j <- if j + 1 < Array.length key then j + 1 else 0
        for k = 1 to N - 1 do
            Array.set mt i <|
                (Array.get mt i ^^^
                    ((Array.get mt (i - 1) ^^^
                        (Array.get mt (i - 1) >>> 30)
                ) * 1566083941u)) - uint i
            if i + 1 < N then
                i <- i + 1
            else
                i <- 1
                Array.set mt 0 <| Array.last mt
        Array.set mt 0 0x80000000u

    (* generate N words at one time *)
    let inline nextState mt =
        for kk = 0 to N - M - 1 do
            let y = (Array.get mt kk &&& UpperMask) |||
                    (Array.get mt (kk + 1) &&& LowerMask)
            Array.set mt kk <|
                Array.get mt (kk + M) ^^^
                (y >>> 1) ^^^
                Array.get mag01 (int (y &&& 0x1u))
        for kk = N - M to N - 2 do
            let y = (Array.get mt kk &&& UpperMask) |||
                    (Array.get mt (kk + 1) &&& LowerMask)
            Array.set mt kk <|
                Array.get mt (kk + (M - N)) ^^^
                (y >>> 1) ^^^
                Array.get mag01 (int (y &&& 0x1u))
        let y = (Array.last mt &&& UpperMask) |||
                (Array.head mt &&& LowerMask)
        Array.set mt (N - 1) <|
            Array.get mt (M - 1) ^^^
            (y >>> 1) ^^^
            Array.get mag01 (int (y &&& 0x1u))

    (* generates a random number on [0,0xffffffff]-interval *)
    let inline genrandUint32 mt mti =
        if !mti = N then
            nextState mt
            mti := 0
        let y = Array.get mt !mti
        incr mti
        (* Tempering *)
        y ^^^ (y >>> 11)
        |> fun y -> y ^^^ ((y <<< 7) &&& 0x9d2c5680u)
        |> fun y -> y ^^^ ((y <<< 15) &&& 0xefc60000u)
        |> fun y -> y ^^^ (y >>> 18)

    (* generates a random number on [0,0x7fffffff]-interval *)
    let inline uint31 v = v >>> 1

    (* generates a random number on [0,1]-real-interval *)
    let inline real1 v =
        double v * (1.0 / 4294967295.0)
        (* divided by 2^32-1 *)

    (* generates a random number on [0,1)-real-interval *)
    let inline real2 v =
        double v * (1.0 / 4294967296.0)
        (* divided by 2^32 *)

    (* generates a random number on (0,1)-real-interval *)
    let inline real3 v =
        (double v + 0.5) * (1.0 / 4294967296.0)
        (* divided by 2^32 *)

    (* generates a random number on [0,1) with 53-bit resolution *)
    let inline res53 a b =
        let a = a >>> 5
        let b = b >>> 6
        (double a * 67108864.0 + double b) * (1.0 / 9007199254740992.0)


type MersenneTwister (seed: uint) =
    class
        let mt = Binds.initBySeed seed
        let mti = ref Binds.N
        let genrandUint32 () = Binds.genrandUint32 mt mti
        member _.GenrandUint32 () : uint = genrandUint32()
        member _.GenrandUint31 () : uint = Binds.uint31 (genrandUint32())
        member _.GenrandReal1 () : double = Binds.real1 (genrandUint32())
        member _.GenrandReal2 () : double = Binds.real2 (genrandUint32())
        member _.GenrandReal3 () : double = Binds.real3 (genrandUint32())
        member _.GenrandRes53 () : double =
            let a = genrandUint32()
            let b = genrandUint32()
            Binds.res53 a b
        member _.SetSeed(seed: uint) : unit =
            mti := Binds.N
            Seq.unfold Binds.genArray (1u, seed)
            |> Seq.iteri (Array.set mt)
        member this.SetSeed(key: uint array) : unit =
            this.SetSeed(19650218u)
            Binds.mergeKey mt key
        member private _.Mt with get () = mt
        new () = MersenneTwister(Binds.DefaultSeed)
        new (key: uint array) as this =
            MersenneTwister(19650218u)
            then
                Binds.mergeKey this.Mt key
    end


namespace Mt19937ar.Module

    module MersenneTwister =
        let inline initByDefaultSeed () = new Mt19937ar.MersenneTwister()
        let inline init (seed: uint) = new Mt19937ar.MersenneTwister(seed)
        let inline initByArray (key: uint array) = new Mt19937ar.MersenneTwister(key)
        let inline setSeed (mt: Mt19937ar.MersenneTwister) (seed: uint) = mt.SetSeed(seed)
        let inline setSeedByArray (mt: Mt19937ar.MersenneTwister) (key: uint array) = mt.SetSeed(key)
        let inline uint32 (mt: Mt19937ar.MersenneTwister) = mt.GenrandUint32()
        let inline uint31 (mt: Mt19937ar.MersenneTwister) = mt.GenrandUint31()
        let inline real1 (mt: Mt19937ar.MersenneTwister) = mt.GenrandReal1()
        let inline real2 (mt: Mt19937ar.MersenneTwister) = mt.GenrandReal2()
        let inline real3 (mt: Mt19937ar.MersenneTwister) = mt.GenrandReal3()
        let inline res53 (mt: Mt19937ar.MersenneTwister) = mt.GenrandRes53()
        let inline genrandUint32 mt = uint32 mt
        let inline genrandUint31 mt = uint31 mt
        let inline genrandReal1 mt = real1 mt
        let inline genrandReal2 mt = real2 mt
        let inline genrandReal3 mt = real3 mt
        let inline genrandRes53 mt = res53 mt


namespace Mt19937ar.Type

    module Immutable =
        type MersenneTwister =
            | MersenneTwister of mt : uint array * mti : int

    module Mutable =
        type MersenneTwister =
            | MersenneTwister of mt : uint array * mti : int ref


namespace Mt19937ar.Immutable

    module MersenneTwister =

        open Mt19937ar.Type.Immutable

        let toMutable (MersenneTwister (mt, mti)) =
            let mt = Array.copy mt
            let mti = ref mti
            Mt19937ar.Type.Mutable.MersenneTwister (mt, mti)

        let nextState (MersenneTwister (mt, mti)) =
            if mti + 1 < Mt19937ar.Binds.N then
                MersenneTwister (mt, mti + 1)
            else
                let mt = Array.copy mt
                Mt19937ar.Binds.nextState mt
                MersenneTwister (mt, 0)

        let init (seed: uint) =
            let mt = Mt19937ar.Binds.initBySeed seed
            let mti = Mt19937ar.Binds.N
            MersenneTwister (mt, mti) |> nextState

        let useCache : bool ref = ref false

        let private defautSeedMt : MersenneTwister option ref = ref None

        let private initByDefaultSeedWithCache () =
            match !defautSeedMt with
            | Some mt -> mt
            | None ->
                let mt = init Mt19937ar.Binds.DefaultSeed
                defautSeedMt := Some mt
                mt

        let initByDefaultSeed () =
            if !useCache then
                initByDefaultSeedWithCache()
            else
                init Mt19937ar.Binds.DefaultSeed

        let private baseMtArray : uint array option ref = ref None

        let private getBaseMtArray () =
            match !baseMtArray with
            | Some mt -> mt
            | None ->
                let mt = Mt19937ar.Binds.initBySeed 19650218u
                baseMtArray := Some mt
                mt

        let private initByArrayWithCache (key: uint array) =
            let mt = getBaseMtArray() |> Array.copy
            Mt19937ar.Binds.mergeKey mt key
            let mti = Mt19937ar.Binds.N
            MersenneTwister (mt, mti) |> nextState

        let initByArray (key: uint array) =
            if !useCache then
                initByArrayWithCache key
            else
                let mt = Mt19937ar.Binds.initBySeed 19650218u
                Mt19937ar.Binds.mergeKey mt key
                let mti = Mt19937ar.Binds.N
                MersenneTwister (mt, mti) |> nextState

        let setSeed (MersenneTwister _) (seed: uint) =
            init seed

        let setSeedByArray (MersenneTwister _) (key: uint array) =
            initByArray key

        let uint32 (MersenneTwister (mt, mti)) =
            Mt19937ar.Binds.genrandUint32 mt (ref mti)

        let uint31 mt = Mt19937ar.Binds.uint31 (uint32 mt)
        let real1 mt = Mt19937ar.Binds.real1 (uint32 mt)
        let real2 mt = Mt19937ar.Binds.real2 (uint32 mt)
        let real3 mt = Mt19937ar.Binds.real3 (uint32 mt)
        let res53 mt =
            let a = uint32 mt
            let b = uint32 (nextState mt)
            Mt19937ar.Binds.res53 a b

        let inline genrandUint32 mt = uint32 mt
        let inline genrandUint31 mt = uint31 mt
        let inline genrandReal1 mt = real1 mt
        let inline genrandReal2 mt = real2 mt
        let inline genrandReal3 mt = real3 mt
        let inline genrandRes53 mt = res53 mt


namespace Mt19937ar.Mutable

    module MersenneTwister =

        open Mt19937ar.Type.Mutable

        let toImmutable (MersenneTwister (mt, mti)) =
            let mt = Array.copy mt
            let mti = !mti
            Mt19937ar.Type.Immutable.MersenneTwister (mt, mti)

        let copy (MersenneTwister (mt, mti)) =
            let mt = Array.copy mt
            let mti = ref !mti
            MersenneTwister (mt, mti)

        let init (seed: uint) =
            let mt = Mt19937ar.Binds.initBySeed seed
            let mti = ref Mt19937ar.Binds.N
            MersenneTwister (mt, mti)

        let initByDefaultSeed () = init Mt19937ar.Binds.DefaultSeed

        let initByArray (key: uint array) =
            let mt = Mt19937ar.Binds.initBySeed 19650218u
            Mt19937ar.Binds.mergeKey mt key
            let mti = ref Mt19937ar.Binds.N
            MersenneTwister (mt, mti)

        let setSeed (MersenneTwister (mt, mti)) (seed: uint) =
            mti := Mt19937ar.Binds.N
            Seq.unfold Mt19937ar.Binds.genArray (1u, seed)
            |> Seq.iteri (Array.set mt)

        let setSeedByArray (MersenneTwister (mt, mti)) (key: uint array) =
            mti := Mt19937ar.Binds.N
            Seq.unfold Mt19937ar.Binds.genArray (1u, 19650218u)
            |> Seq.iteri (Array.set mt)
            Mt19937ar.Binds.mergeKey mt key

        let uint32 (MersenneTwister (mt, mti)) =
            Mt19937ar.Binds.genrandUint32 mt mti

        let uint31 mt = Mt19937ar.Binds.uint31 (uint32 mt)
        let real1 mt = Mt19937ar.Binds.real1 (uint32 mt)
        let real2 mt = Mt19937ar.Binds.real2 (uint32 mt)
        let real3 mt = Mt19937ar.Binds.real3 (uint32 mt)
        let res53 mt =
            let a = uint32 mt
            let b = uint32 mt
            Mt19937ar.Binds.res53 a b

        let inline genrandUint32 mt = uint32 mt
        let inline genrandUint31 mt = uint31 mt
        let inline genrandReal1 mt = real1 mt
        let inline genrandReal2 mt = real2 mt
        let inline genrandReal3 mt = real3 mt
        let inline genrandRes53 mt = res53 mt


(*
 *  疑似乱数生成機(RNG)  移植(Porting)
 *  Information of Original Source
 *  Mersenne Twister with improved initialization (2002)
 *  http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/mt.html
 *  http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/MT2002/mt19937ar.html
 *)
// = 移植元ラインセンス (License of Original Source) =======================================================
// ======================================================================
(*
   A C-program for MT19937, with initialization improved 2002/1/26.
   Coded by Takuji Nishimura and Makoto Matsumoto.

   Before using, initialize the state by using init_genrand(seed)
   or init_by_array(init_key, key_length).

   Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,
   All rights reserved.

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

     1. Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.

     2. Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

     3. The names of its contributors may not be used to endorse or promote
        products derived from this software without specific prior written
        permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


   Any feedback is very welcome.
   http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/emt.html
   email: m-mat @ math.sci.hiroshima-u.ac.jp (remove space)
*)
// ======================================================================
